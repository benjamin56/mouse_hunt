import sys
import os
import shutil
import platform
from cx_Freeze import setup, Executable

VERSION = 'v1.1'
GAME_NAME = 'mouse_hunt'


if __name__ == '__main__':
    sys.argv[:] = [sys.argv[0], 'build']
    os_family = {'Windows': 'win', 'Darwin': 'mac', 'Linux': 'linux'}[platform.system()]

    buildOptions = dict(
        packages=["pyglet"],
        excludes=[],
        include_files=[('assets', 'assets')])

    target = Executable(
        'game.py',
        base={'win': 'Win32GUI', 'mac': None, 'linux': None}[os_family],
        targetName='game',
        icon='assets/icon.' + {'win': 'ico', 'mac': 'icns', 'linux': 'ico'}[os_family],
        copyright='Mikhail Shubin',
    )

    setup(
        name=GAME_NAME,
        version=VERSION,
        description='Mouse Hunt',
        options={'build_exe': buildOptions},
        executables=[target]
        )

    # zip files
    build_dir = 'build/'+[folder for folder in os.listdir('build') if folder.startswith('exe')][0]
    shutil.make_archive(f'build/{GAME_NAME}_{os_family}_{VERSION}', 'zip', build_dir)

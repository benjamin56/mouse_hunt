from PIL import Image

img = Image.open('assets/sprite.png')
bg = Image.open('screens/bg.png').convert('RGBA')

IMAGES = []

def read_sprite(x, y):
    crop = img.crop([0+32*x, 2+32*y, 30+32*x, 32+32*y])
    crop = crop.resize([480, 480]).convert('RGBA')
    return crop

def make_frame(name, x, y):
    frame = bg.copy()
    crop = read_sprite(x, y)
    frame.paste(crop.convert('RGB'), (35, -40), mask=crop)
    #frame.save(f'screens/frame{name}.png')
    IMAGES.append(frame.copy())

for i in range(5):
    make_frame(0, 0, 1)
for i in range(10):
    make_frame(i+1, i, 3)
for i in range(10):
    make_frame(i+11, i, 5)
for i in range(5):
    make_frame(0, 0, 1)

IMAGES[0].save('screens/cover.gif', save_all=True, append_images=IMAGES[1:], optimize=False, duration=100, loop=0)

print('done')